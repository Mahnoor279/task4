package com.example.task4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    Button submit;
    EditText firstName;
    EditText lastName;
    EditText email;

    String fnStr;
    String lnStr;
    String eStr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        submit =  findViewById(R.id.button);
        firstName = findViewById(R.id.firstName);
        lastName =  findViewById(R.id.lastName);
        email =  findViewById(R.id.email);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fnStr = firstName.getText().toString();
                lnStr = lastName.getText().toString();
                eStr = email.getText().toString();
                Intent intent = new Intent(MainActivity.this, NextActivity.class);
                intent.putExtra("first", fnStr);
                intent.putExtra("last", lnStr);
                intent.putExtra("email", eStr);
                startActivity(intent);
            }
        });
    }
}